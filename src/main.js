/**
 *
 *
 * @returns {Promise<void>}
 */
async function main()  {
    // only on the wifi page
    if (window.location.hash === '#/wifi') {
        var loaded = false;
        // as the vodafone station frontend is a webapp it is not clear when the DOM is fully loaded
        while (!loaded) {
            // slowing doing trying
            await new Promise(r => setTimeout(r, 200));

            // the button to change the WLAN password
            let btn = document.getElementById('ChangePwd1');
            // start if it exists
            if (btn !== null) {
                // change the name to deactivate the broken password change modal of the vodafone station
                btn.id = 'vpb_hacked';
                // register a new event listener
                btn.addEventListener(
                    "click",
                    function() {
                        // get the user input / wlan password
                        var password = window.prompt("WLAN Password: ", "").trim();
                        if (password.length > 7) {
                            document.getElementById('KeyPassphrase1').value = password;
                        } else {
                            alert('Password needs at least a length of 8 chars (mind. 8 Zeichen)');
                        }
                    }
                );
                loaded = true;
                console.log("Vodafone-PasswordCheck-Bypass loaded");
            }
        }
    }
}

// run initially without a popstate event
main();

// register a check on every browser history aka address bar change
(async function() {
    window.addEventListener(
        'popstate',
        main
    );
})();