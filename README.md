
![](icons/vpb-96.png)
# Vodafone-PasswordCheck-ByPass

The "vodafone station" cable router has a pretty annoying bug. The popup to change the WLAN password has a broken check so you can't enter almost never a real secure password because it evaluates it incorrectly. With this addon this popup is interrupted / replaced with another prompt bypass this 
error and set whatever password you want that has at least a length of 8 chars.

## Instructions

1. 
![](readme/menu_navigation.png)

2.
![](readme/new_password_input_field.png)



## License
GNU General Public License v3.0

